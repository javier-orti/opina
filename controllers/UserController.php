<?php

function save_user() //Save in DB new user
{
    $user = new User();
    $data = [
        'usu_email' => $_POST['usu_email'],
        'usu_password' => $_POST['usu_password'],
        'usu_nombre' => $_POST['usu_nombre'],
        'usu_ape1' => $_POST['usu_ape1'],
        'usu_ape2' => $_POST['usu_ape2'],
        'codeAccess' =>$_POST['codeAccess'],
        'end_year' => $_POST['end_year']
    ];
    return $user->set_register_users($data);
}


function login_user($email,$pass) //LOGIN User
{
    $user = new User();
    $data = [
        'usu_email' => $email,
        'pass' => $pass,
    ];
   return $user->login_user($data);

}
function search_code($code,$year) //LOGIN User
{
    $user = new User();
   return $user->search_code_action($code,$year);

}

function list_users()
{
    $user = new User();
    return $user->list_users_action();
}

function list_users_send($id)
{
    $user = new User();
    return $user->users_send_form_action($id);

}
?>