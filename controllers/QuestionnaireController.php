<?php

function list_questionnaire()
{
    $questionnaire = new Questionnaire();
    return $questionnaire->list_questionnaire_action();
}

function edit_question($id)
{
    $questionnaire = new Questionnaire();
    return $questionnaire->edit_question_action($id);

}

function edit_answers($id)
{
    $questionnaire = new Questionnaire();
    return $questionnaire->edit_answers_action($id);
}

function  update_question_answer($question,$anwers)
{
    $questionnaire = new Questionnaire();
    echo $question['id'];
    $questionnaire->update_question_answer_action($question,$anwers);

}


function insert_question_answers()
{
    $questionnaire = new Questionnaire();
    $inputsAnswer = count($_POST);
    $data = [];
    array_push($data, ['question' => $_POST['question']]);
    for ($i = 1; $i < $inputsAnswer; $i++) {
        $val = (string)$i;
        array_push($data, [$val => $_POST[$val]]);
    }
    $questionnaire->insert_question_action($data);
}

function list_answers($id)
{
    $questionnaire = new Questionnaire();
    return $questionnaire->list_answers($id);
}

function action_questionnaire($idUser)
{
    $questionnaire = new Questionnaire();
    $questionnaire->success_questionnaire_action($idUser);
}

function check_form($idUser)
{
    $questionnaire = new Questionnaire();
    return $questionnaire->checkForm_action($idUser);
}

?>