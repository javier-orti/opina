<?php

/*
	CRUD creado por Javier Orti
	Contacto: javier@tba.es
*/

class User extends db
{

    public function randomCodeAccess($id)
    {
        $permitted_chars = '0123456789ABCDEFGHIJKLMNÑOPQRSTUVWXYZ';
        return $id . 'OP' . substr(str_shuffle($permitted_chars), 0, 4);
    }

    public function randomCodeEmbajador($id)
    {
        $permitted_chars = '0123456789ABCDEFGHIJKLMNÑOPQRSTUVWXYZ';
        return substr(str_shuffle($permitted_chars), 0, 4) . 'OP' . $id;
    }

    public function set_register_users($data)
    {
        $hash = password_hash($data['usu_password'], PASSWORD_DEFAULT);
        $SQL0 = 'SELECT * from com_alumnos where codembajador=? or (code_access=? and activado=?)'; //first search codembajador  of invitation with other user
        $activado = 0;
        $result0 = $this->connect()->prepare($SQL0);
        $result0->bindParam(1, $data['codeAccess']);
        $result0->bindParam(2, $data['codeAccess']);
        $result0->bindParam(3, $activado);
        $result0->execute();
        $row = $result0->fetch();

        try {

            if ($result0->rowCount()) {
                if ($row['code_access'] == $data['codeAccess'] && $row['activado'] == 0) {
                    $codeAccess = $data['codeAccess'];
                    $codeEmbaja = $this->randomCodeEmbajador($row['id']);
                    $SQL2 = "UPDATE com_alumnos SET code_access=?,codembajador=?,usu_level=?,activado=?,pass=?,nombre=?,ape1=?,ape2=?,email=?,codusuario=? WHERE id=?";
                    $level = "1";
                    $activado = 1;
                    $result2 = $this->connect()->prepare($SQL2);
                    $result2->bindParam(1, $codeAccess);
                    $result2->bindParam(2, $codeEmbaja);
                    $result2->bindParam(3, $level);
                    $result2->bindParam(4, $activado);
                    $result2->bindParam(5, $hash);
                    $result2->bindParam(6, $data['usu_nombre']);
                    $result2->bindParam(7, $data['usu_ape1']);
                    $result2->bindParam(8, $data['usu_ape2']);
                    $result2->bindParam(9, $data['usu_email']);
                    $result2->bindParam(10, $data['usu_codusuario']);
                    $result2->bindParam(11, $row['id']);
                    $result2->execute() or die('error');

                } else {
                    $SQL = 'INSERT INTO com_alumnos (nombre,ape1,ape2,email,pass,year,registered,codusuario) VALUES (?,?,?,?,?,?,?,?)';
                    $result = $this->connect()->prepare($SQL);
                    $registered = 1;
                    $result->bindParam(1, $data['usu_nombre']);
                    $result->bindParam(2, $data['usu_ape1']);
                    $result->bindParam(3, $data['usu_ape2']);
                    $result->bindParam(4, $data['usu_email']);
                    $result->bindParam(5, $hash);
                    $result->bindParam(6, $data['end_year']);
                    $result->bindParam(7, $registered);
                    $result->bindParam(8, $data['usu_codusuario']);
                    $result->execute() or die('no se ha podido guardar');

                    //Generate ACCESS USER and SAVE TO USER
                    $lasId = $this->connect->lastInsertId();
                    $codeAccess = $data['codeAccess'];
                    $codeEmbaja = $this->randomCodeEmbajador($lasId);
                    $SQL2 = "UPDATE com_alumnos SET code_access=?,codembajador=?,usu_level=? WHERE id=?";
                    $level = "2";
                    $result2 = $this->connect()->prepare($SQL2);
                    $result2->bindParam(1, $codeAccess);
                    $result2->bindParam(2, $codeEmbaja);
                    $result2->bindParam(3, $level);
                    $result2->bindParam(4, $lasId);
                    $result2->execute() or die('error');
                }


                return 'success';
            } else {
                return 'error';
            }


        } catch (Exception $e) {
            die('Error al guardarse el usuario ' . $e->getMessage());
        } finally {
            $result = null;
        }
    }

    public function login_user($data)
    {

        try {
            /**
             * TODO: Aquí consultamos la API DE STEVE si EXISTE EL USUARIO
             * si existe lo grabamos en BBDD si no el usuario ya hara el formulario de registro usando un codigo embajador de otro usuario
             *
             * TODO: RECOGIDO DE LA API (Hacer una función) y  LLAMAMOS LA FUNCIÓN   set_register_users($data) o creamos una de registro gemela DONDE data SERA UN ARRAY de los datos de la API
             * ¡ojo con registrar el nivel 1 o 2 de usuario!
             **/

            $res = $this->apilogin($data['usu_email'], $data['pass']);
            $row = [];
            if ($res != 'KO') {
                $datos = explode(";", $res . ';codeAccess=' . $_GET['code'] . ';end_year=' . $_GET['end_year'] . ';usu_password=' . $data['pass']);
                foreach ($datos as $element) {
                    $datos1 = explode("=", $element);
                    $row[$datos1[0]] = utf8_encode($datos1[1]);
                }
                $SQL = 'SELECT * FROM com_alumnos WHERE codusuario= ?';//if not exists user execute set_register_users
                $re=$this->connect()->prepare($SQL);
                $re->bindParam(1, $row['usu_codusuario']);
                $re->execute();
                if($re->rowCount()==0){
                    return $this->set_register_users($row);
                }

            } else {
                $SQL = 'SELECT * FROM com_alumnos WHERE email= ?';
                $result = $this->connect()->prepare($SQL);
                $result->bindParam(1, $data['usu_email']);
                $result->execute();
                while ($fila = $result->fetch(PDO::FETCH_ASSOC)) {
                    if (password_verify($data['pass'], $fila['pass'])) {

                        if ($fila['usu_level'] != 0) {//if user not admin
                            if ($fila['codembajador'] == null) { //if no exists codeEmbajador in users level 1
                                $SQL3 = 'UPDATE com_alumnos SET codembajador=?,activado=? WHERE id=?';
                                $active = 1;
                                $codeEmbaja = $this->randomCodeEmbajador($fila['id']);
                                $result3 = $this->connect()->prepare($SQL3);
                                $result3->bindParam(1, $codeEmbaja);
                                $result3->bindParam(2, $active);
                                $result3->bindParam(3, $fila['id']);
                                $result3->execute();
                                $_SESSION['code_em'] = $codeEmbaja;
                            } else {
                                $SQL1 = 'SELECT * FROM com_alumnos WHERE code_access= ?';//Count invitations
                                $result1 = $this->connect()->prepare($SQL1);
                                $result1->bindParam(1, $fila['codembajador']);
                                $result1->execute();
                                $_SESSION['invitations'] = $result1->rowCount();
                                $_SESSION['code_em'] = $fila['codembajador'];
                                $row = $result1->fetchAll();
                                $_SESSION['usersInvitation'] = $row;
                            }
                            if ($fila['registered'] == null) {
                                $_SESSION['registered'] = 0;
                            }
                        }
                        $_SESSION['nombre'] = $fila['nombre'];
                        $_SESSION['id'] = $fila['id'];
                        $_SESSION['usu_level'] = $fila['usu_level'];


                    }
                }
            }


            return $res;

        } catch (Exception $e) {
            die('Error al guardarse el usuario ' . $e->getMessage());
        } finally {
            $result = null;
        }
    }

    public function list_users_action()
    {
        try {
            // $SQL = 'SELECT * FROM com_alumnos inner join response_questions on com_alumnos.id=response_questions.id_alum';
            $SQL = 'SELECT * FROM com_alumnos';
            $result = $this->connect()->prepare($SQL);
            $result->execute();
            return $result->fetchAll();

        } catch (Exception $e) {
            die('Error al guardarse el usuario ' . $e->getMessage());
        } finally {
            $result = null;
        }
    }

    public function users_send_form_action($id)
    {
        try {
            $SQL = 'SELECT * FROM  response_questions where id_alum=?';
            $result = $this->connect()->prepare($SQL);
            $result->bindParam(1, $id);
            $result->execute();
            return $result->fetch();

        } catch (Exception $e) {
            die('Error al guardarse el usuario ' . $e->getMessage());
        } finally {
            $result = null;
        }
    }

    public function search_code_action($code, $year)
    {
        try {
            $SQL = 'SELECT * FROM  com_alumnos where  codembajador=?  or (code_access=? and activado=?) ';
            $activado = 0;
            $result = $this->connect()->prepare($SQL);
            $result->bindParam(1, $code);
            $result->bindParam(2, $code);
            $result->bindParam(3, $activado);
            $result->execute();
            if ($result->rowCount()) {

                $SQL2 = 'UPDATE com_alumnos set year=?,registered=? where id=?';//update  year
                $registered = 1;
                $result2 = $this->connect()->prepare($SQL2);
                $result2->bindParam(1, $year);
                $result2->bindParam(2, $registered);
                $result2->bindParam(3, $result->fetch()['id']);
                $result2->execute();

            }
            if (!$result->rowCount()) {
                $SQL3 = 'SELECT * FROM  com_alumnos where codembajador=?';
                $result3 = $this->connect()->prepare($SQL3);
                $result3->bindParam(1, $code);
                $result3->execute();
                return $result3->rowCount();
            } else {
                return $result->rowCount();
            }


        } catch (Exception $e) {
            die('Error al guardarse el usuario ' . $e->getMessage());
        } finally {
            $result = null;
        }
    }

    public function apilogin($email, $pass)
    {
        $response=$this->datosApi('http://www.esteve.es/EsteveFront/Login.do?op=LEX',['usu_email'=>$email,'usu_password'=>$pass]);
        return $response;
//return 'KO';
       // return 'usu_codusuario=181782;usu_email=javier@tba.es;usu_nombre=javier;usu_ape1=orti;usu_ape2=marina;usu_perfil=OT;usu_especialidad=0;usu_numcolegiado=;usu_pais=042;usu_provincia=08;usu_poblacion=217;usu_ciudad=Sant Joan Despi;usu_direccion=Calle rius i taulet;usu_cp=08970;usu_telefono=;usu_dni=23280369E;usu_fax=23280369E;usu_empresa=calle les torres 10;usu_tipo=E;mailing=S';
    }

    function datosApi($url, $fields)
    {

        foreach ($fields as $key => $value) {
            $fields_string .= $key . '=' . $value . '&';
        }
        rtrim($fields_string, '&');
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_POST, count($fields));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        curl_close($ch);
        return $response;
    }


}


?>