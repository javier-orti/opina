<?php

/*
	Created Class Questionaire  by Javier Orti
	Contacto: javier@tba.es
*/

class Questionnaire extends db
{

    public function list_questionnaire_action()
    {
        try {
            $SQL = 'SELECT * from questionnaire';
            $result = $this->connect()->prepare($SQL);
            $result->execute() or die('no se ha podido guardar');
            $listQuestions = $result->fetchAll();
            return $listQuestions;
        } catch (Exception $e) {
            die('Error al guardarse el usuario ' . $e->getMessage());
        } finally {
            $result = null;
        }
    }

    public function edit_question_action($id)
    {
        try {
            $SQL = 'SELECT * from questionnaire where id=?';
            $result = $this->connect()->prepare($SQL);
            $result->bindParam(1, $id);
            $result->execute() or die('no se ha podido realizar la consulta');
            $question = $result->fetch();
            return $question;
        } catch (Exception $e) {
            die('Error al buscar la pregunta: ' . $e->getMessage());
        } finally {
            $result = null;
        }
    }

    public function edit_answers_action($id)
    {
        try {
            $SQL = 'SELECT * from answers where id_question=?';
            $result = $this->connect()->prepare($SQL);
            $result->bindParam(1, $id);
            $result->execute() or die('no se ha podido realizar la consulta');
            $answers = $result->fetchAll();
            return $answers;
        } catch (Exception $e) {
            die('Error al buscar las respuestas: ' . $e->getMessage());
        } finally {
            $result = null;
        }
    }

    public function update_question_answer_action($question, $answers)
    {
        try {
            $valueIdQ=(string)$question['id'];
            $SQL = 'UPDATE questionnaire SET question=? WHERE id=?';
            $result = $this->connect()->prepare($SQL);
            $result->bindParam(1, $_POST[$valueIdQ]);
            $result->bindParam(2, $_POST['id']);
            $result->execute() or die('no se ha podido realizar la actualización');

            foreach ($answers as $answer) {
                if ($answer['id']) {
                    $valueId=(string)$answer['id'];
                    $SQL2= 'UPDATE answers SET answer=? WHERE id_question=? and id=?';
                    $result2 = $this->connect()->prepare($SQL2);
                    $result2->bindParam(1, $_POST[$valueId]);
                    $result2->bindParam(2, $question['id']);
                    $result2->bindParam(3, $answer['id']);
                    $result2->execute() or die('no se ha podido realizar la actualización');

                }
            }


        } catch (Exception $e) {
            die('Error al buscar las respuestas: ' . $e->getMessage());
        } finally {
            $result = null;
        }

    }

    public
    function insert_question_action($data)
    {

        try {
            $count = 0;
            $lasQuestion = null;
            foreach ($data as $dat) {
                if ($dat['question']) {
                    $SQL = 'INSERT INTO questionnaire (question) VALUES (?)';
                    $result = $this->connect()->prepare($SQL);
                    $result->bindParam(1, $dat['question']);
                    $result->execute() or die('error al grabar la pregunta');
                    $lasQuestion = $this->connect->lastInsertId();
                } else {
                    if ($lasQuestion != null) {
                        $val = (string)$count;
                        $value = $dat[$val];
                        echo $value;
                        $SQL2 = 'INSERT INTO answers (answer,id_question) VALUES (?,?)';
                        $result2 = $this->connect()->prepare($SQL2);
                        $result2->bindParam(1, $value);
                        $result2->bindParam(2, $lasQuestion);
                        $result2->execute() or die('error al grabar la respuesta');
                    }

                }
                $count++;
            }
        } catch (Exception $e) {
            die('Error al guardado : ' . $e->getMessage());
        } finally {
            $result = null;
        }


    }


    public
    function list_answers($id)
    {
        try {
            $SQL = 'SELECT * from answers where id_question=?';
            $resulta = $this->connect()->prepare($SQL);
            $resulta->bindParam(1, $id);
            $resulta->execute();
            $listAnswers = $resulta->fetchAll();
            return $listAnswers;

        } catch (Exception $e) {
            die('Error al guardarse el usuario ' . $e->getMessage());
        } finally {
            $resulta = null;
        }
    }

    public
    function checkForm_action($idUser)
    {
        try {
            $SQL = 'SELECT * from response_questions where id_alum=?';
            $resulta = $this->connect()->prepare($SQL);
            $resulta->bindParam(1, $idUser);
            $resulta->execute();
            return $resulta->rowCount();

        } catch (Exception $e) {
            die('Error al guardarse el usuario ' . $e->getMessage());
        } finally {
            $resulta = null;
        }
    }


    public
    function success_questionnaire_action($idUser)
    {
        try {
            $SQL = 'SELECT * from com_alumnos where id=?';
            $result = $this->connect()->prepare($SQL);
            $result->bindParam(1, $idUser);
            $result->execute() or die('no existe el usuario');
            if ($result->rowCount()) {
                $SQL2 = 'SELECT * from questionnaire';
                $result2 = $this->connect()->prepare($SQL2);
                $result2->execute();

                $succes = false;
                $answers = [];
                foreach ($result2->fetchAll() as $row) {
                    if (!isset($_POST[$row['id']])) {
                        $succes = true;
                    } else {
                        array_push($answers, [$row['id'] => $_POST[$row['id']]]);
                    }
                }
                $json = json_encode($answers);
                if (!$succes) {
                    $SQL3 = "INSERT INTO response_questions (id_alum,response_questions) value (?,?)";
                    $result3 = $this->connect()->prepare($SQL3);
                    $result3->bindParam(1, $idUser);
                    $result3->bindParam(2, $json);
                    $result3->execute() or die('error');
                }
            }
        } catch (Exception $e) {
            die('Error al enviarse el formulario ' . $e->getMessage());
        } finally {
            $result = null;
        }

    }


}


?>