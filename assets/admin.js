$(document).ready(function () {

    //Todo:LOAD FOR ADMIN

    //Todo: List Questions
    $('#list-questions-admin-action').click(function (e) {
        e.preventDefault();
        location.href = "?actionA=questions";
    });

    $('#add-question-admin').click(function (e) {
        e.preventDefault();
        location.href = "?actionA=add";
    });

    //Todo: ADD QUESTION AND ANSWERS FOR QUESTIONNAIRE
    $('#add-answer-admin').click(function () {
        $.ajax({
            url: "?actionA=add",
            contentType: false,
            cache: false,
            processData: false,
            async: false,
            success: function (response) {
                var id = 0;
                var t= $('#content-answers textarea:last').attr('name');
                if(t){
                  id=t;
                }
                id++;
                let text = id.toString();
                $('#content-answers').append('     <div class="ans row mt-2">\n' +
                    '                <div class="col-md-12">\n' +
                    '                    <textarea name="' + text + '" class="form-control" rows=2\n' +
                    '                              placeholder="Escribe aquí la respuesta"></textarea>\n' +
                    '                </div>\n' +
                    '            </div>');
                $('html,body').animate({
                    scrollTop: $("#body-opina").offset().top
                });

            }
        });
    });

    $('#delete-answer-admin').click(function () {
        $.ajax({
            url: "?actionA=add",
            contentType: false,
            cache: false,
            processData: false,
            async: false,
            success: function (response) {
                $(".ans:last").remove();
                $('html,body').animate({
                    scrollTop: $("#body-opina").offset().top
                });
            }
        });
    });
    $('form[name="add-question"]').submit(function (e) {
        e.preventDefault();
        $.ajax({
            url: "?actionA=save-question",
            data: new FormData(this),
            type: 'POST',
            contentType: false,
            cache: false,
            processData: false,
            async: true,
            success: function (response) {
                alert('Se ha grabado con éxito la pregunta  y respuestas');
                location.href = "?actionA=questions";
            }
        });
    });

    //Todo: EDIT QUESTION AND ANSWERS FOR QUESTIONNAIRE
    $('form[name="update-question-form"]').submit(function (e) {
        e.preventDefault();
        $.ajax({
            url: "?actionA=updateQuestionnarieform",
            data: new FormData(this),
            type: 'POST',
            contentType: false,
            cache: false,
            processData: false,
            async: true,
            success: function (response) {

            }
        });
    });



    //Todo: List User

     $('#list-user-admin-action').click(function () {
         location.href = "?actionA=list-users";
    });








});

