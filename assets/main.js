$(document).ready(function () {

    //Todo:LOAD FOR BODY

    //TODO: Load form regiter new user
    $('#register-user,#btn-user-new').click(function () {
        var code = '';
        var end_year = '';
        if ($('#code').html()) {
            code = $('#code').html();
        }
        if ($('#end_year').html()) {
            end_year = $('#end_year').html();
        }
        $('#body-opina').load('views/home/user_register.php?code=' + code + '&end_year=' + end_year);


        $('html,body').animate({
            scrollTop: $("#body-opina").offset().top
        }, 1000);
    });



    //TODO: When login user
    $('#login-user,#login-user-now').click(function () {
        // location.reload();
        $('#body-opina').load('views/home/user_login.php');
        $('html,body').animate({
            scrollTop: $("#body-opina").offset().top
        }, 1000);
    });


    //TODO: view For Questionnaire

    $('.questionnaire').click(function () {
        location.href = "?actionQ=list";
    });

    var valores = window.location.search;
    if (valores.indexOf('lista')) {
        $('nav,#logout-user').mouseover(function () {
            $('#pop-pup').css('display', 'block');
        });
    }
    $('#pop-pup').click(function () {
        $('#pop-pup').toggle();
    })


    //TODO : form questionnarie success
    $('form[name="form-questionnaire"]').submit(function (e) {
        e.preventDefault();
        $.ajax({
            url: "?actionQ=success",
            data: new FormData(this),
            type: 'POST',
            contentType: false,
            cache: false,
            processData: false,
            async: true,
            success: function (response) {
                alert("Cuestionario enviado")
                location.href = "?actionQ=info";
            }
        });
    });


    //TODO: login user submit
    $('form[name="login"]').submit(function (e) {
        e.preventDefault();
        var year=getParameter('end_year');
        var code= getParameter('code');
        $.ajax({
            url: "?action=login&code="+code+"&end_year="+year,
            data: new FormData(this),
            type: 'POST',
            contentType: false,
            cache: false,
            processData: false,
            async: true,
            success: function (response) {
                location.href = '?action=login-check';
            }, error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status);
                alert(thrownError);
            }
        });
    });


    //TODO: logout user submit
    $('#logout-user').click(function (e) {
        e.preventDefault();
        logout();
    });

    //TODO: codembajador create link

    $('.btn-codembajador').click(function () {
        //todo : create red social with link
    });

    if(valores.indexOf('codembajador')){
       var d= getParameter('codembajador');
       $('input[name="code"]').attr('value',d);

    }

    $('.btn-users-invitation').click(function () { //show table users invitations
        $('#table-users-invitations').toggle();
    });



    // $(".nav-link").click(function (){
    //
    //     $(this).addClass("active").siblings().removeClass("active");
    //
    // });

});

function logout() {
    $.ajax({
        url: "?action=logout",
        success: function (response) {
            location.href = "/opina";
        }
    });
}

function getParameter(p)
{
    var url = window.location.search.substring(1);
    var varUrl = url.split('&');
    for (var i = 0; i < varUrl.length; i++)
    {
        var parameter = varUrl[i].split('=');
        if (parameter[0] == p)
        {
            return parameter[1];
        }
    }
}