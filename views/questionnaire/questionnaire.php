<div class="container-fluid p-0">


    <fieldset>
        <div class="text-center">
            <img id="pop-pup" class="img-responsive" src="assets/img/popup.png">
        </div>
        <div id="demo" class="carousel slide" data-ride="carousel">


            <!-- The slideshow -->
            <div class="carousel-inner">
                <form method="post" name="form-questionnaire">
                    <input type="hidden" name="user" value="<?php echo $_SESSION['id']; ?>">

                    <?php
                    $i = 0;
                    foreach ($listQuestions

                    as $row) {
                    if ($i == 0){
                    ?>
                    <div class="carousel-item carru-questionnaire active text-center"><?php
                        }else{
                        ?>
                        <div class="carousel-item carru-questionnaire text-center"><?php
                            }
                            ?>
                            <?php echo "<p class='letter-red'>" . $row['question'] . "</p>";
                            $answers = list_answers($row['id']);
                            ?>

                            <ul class="text-left"><?php
                                foreach ($answers as $rowa) {
                                    ?>
                                    <li style="list-style: none">
                                        <div class="radio mb-3">
                                            <input type="radio"
                                                   name="<?php echo $row['id'] ?>" value="<?php echo $rowa['id']; ?>">
                                            <span
                                                    class="ml-2 letter-grey"><?php echo $rowa['answer']; ?></span>
                                        </div>
                                    </li>
                                    <?php
                                }
                                $i++;
                                if (count($listQuestions) == $i) {
                                    ?>
                                    <div class="text-right pt-2">
                                        <button class="btn btn-danger" type="submit" style="font-weight: bold">ENVIAR
                                        </button>
                                    </div>
                                    <?php
                                }
                                ?>
                            </ul>

                        </div>
                        <?php
                        }
                        ?>
                    </div>
                </form>

                <!-- Indicators -->
                <ul class="carousel-indicators carousel-indicators-numbers ">
                    <?php
                    $i = 0;
                    foreach ($listQuestions as $row) {
                        $i++;
                        if($i==1){
                            $active="class='active'";
                        }else{
                            $active=null;
                        }
                        ?>
                        <li data-target="#demo" data-slide-to="<?php echo $i-1;?>" <?php echo $active;?>> <?php echo $i;?> </li>
                        <?php
                    }
                    ?>
                </ul>
            </div>

    </fieldset>
</div>
<div id="control-carousel">
    <!-- Left and right controls -->
    <a class="carousel-control-prev" href="#demo" data-slide="prev">
        <span class="letter-grey"><i class="bi bi-arrow-left-circle-fill"></i> <b>Pregunta anterior</b></span>
    </a>
    <a class="carousel-control-next" href="#demo" data-slide="next">
        <span class="letter-grey"><b>Pregunta siguiente</b> <i class="bi bi-arrow-right-circle-fill"></i>
    </a>
</div>