<?php

if ($_SESSION['usu_level'] == 1) {
    ?>
    <div class=" text-center mt-3 mb-5">
        <div class="container mt-3 mb-3">
            <?php if(check_form($_SESSION['id'])){ ?>
                <p class="letter-red">¡Gracias por darnos tu opinión!</p>
                <?php
            }else{
                ?>
                <p class="letter-red"><b>Responde al cuestionario para poder invitar</b></p>
                <?php
            }?>
        </div>
        <div class="container-fluid info-question-user text-center mb-4 mt-4 pt-5 pb-5">
            <div class="container">
                <p class="letter-red">¿Quieres convertirte en embajador?</p>
                <p class="letter-grey"> Tus invitados podrán participar en un sorteo a un Congreso Nacional o Europeo
                    para completar su formación.<br> Podrás conocer cuantos de tus invitados han participado en OpinA
                    cada
                    vez que entres en la web.</p>

                <p class="letter-grey">Solicita tu <b>código de embajador</b> y compártelo con colegas de profesión de tu misma
                    especialidad.
                </p>

                <p class="letter-grey">Deberán introducir el código para acceder al cuestionario.</p>

            </div>

        </div>
        <?php if(check_form($_SESSION['id'])){
            ?>
            <div class="text-center mt-1">
                <button class="btn btn-danger" id="btn-code-em">CÓDIGO DE EMBAJADOR</button>

                        <a href="?share&code=<?php echo $_SESSION['code_em']; ?>">compartir</a>
            </div>
            <?php
        }else{
            ?>
            <div class="text-center mt-1">
                <a href="?actionQ=list"><button class="btn btn-danger" id="btn-code-em">RESPONDER CUESTIONARIO</button></a>
            </div>
            <?php
        }?>
<!---->
<!--        <h6 class="mt-4"><b>--><?php //echo $_SESSION['code_em']; ?><!--</b></h6>-->

    </div>
    <?php
}


if ($_SESSION['usu_level'] == 2) {
    ?>
    <div class="text-center m-4">
        <?php if(check_form($_SESSION['id'])){ ?>
            <p class="letter-red">¡Gracias por darnos tu opinión!</p>
            <?php
        }else{
            ?>
            <p class="letter-red"><b>Responde al cuestionario para poder invitar</b></p>
            <?php
        }?>
    </div>
    <div class="info-question-user text-center container-fluid pt-4 pb-4">

        <p class="letter-red"><b>¿Quieres convertirte en embajador?</b></p>

        <p class="letter-grey">Tus invitados podrán participar en un sorteo a un Congreso Nacional o Europeo para
            completar su formación.<br> Podrás conocer cuantos de tus invitados han participado en OpinA cada vez que
            entres
            en la web.
        </p>
        <p class="letter-grey">Solicita tu código de embajador y compártelo con colegas de profesión<br> de tu misma
            especialidad.</p>
        <p class="letter-grey"> Deberán introducir el código para acceder al cuestionario.</p>


    </div>
    <?php if(check_form($_SESSION['id'])){
        ?>
        <div class="text-center mt-1">
            <button class="btn btn-danger" id="btn-code-em">CÓDIGO DE EMBAJADOR</button>
            <a href="?codembajador=<?php echo $_SESSION['code_em']; ?>" class="btn-codembajador">compartir</a>
        </div>
        <?php
    }else{
        ?>
        <div class="text-center mt-1">
            <a href="?actionQ=list"><button class="btn btn-danger" id="btn-code-em">RESPONDER CUESTIONARIO</button></a>
        </div>
        <?php
    }?>


    <?php
}

?>
