<script >

    //TODO: new-user submit
    $('form[name="form-new-user"]').submit(function (e) {
        e.preventDefault();
        $.ajax({
            url: "?action=register",
            data: new FormData(this),
            type: 'POST',
            contentType: false,
            cache: false,
            processData: false,
            async: true,
            success: function (response) {
                 alert("Nuevo registro creado.Prueba de hacer login con el correo y contraseña ");
                 location.href = '?success';

            }
        });
    });

</script>
<div class="container-fluid p-5" id="section-new-user">

    <div class="container">
        <p class="letter-red text-center">Regístrate para acceder</p>

        <form name="form-new-user" method="POST" enctype="multipart/form-data">
            <input type="hidden" name="con" value="299">
            <input type="hidden" name="zon" value="34">
            <div class="row row-cols-1">
                <div class="col-12">
                    <input type="email" class="form-control" name="usu_email" placeholder="Correo electrónico*"
                           autocomplete="new-email" required="" aria-required="true" value="insorti@gmail.com">
                </div>
                <div class="col-lg-6">
                    <input type="password" class="form-control" name="usu_password" id="usu_password"
                           placeholder="Contraseña*" autocomplete="new-password" required="" aria-required="true"
                          >
                </div>
                <div class="col-lg-6">
                    <input type="password" class="form-control" name="usu_password2" id="usu_password2"
                           placeholder="Repetir contraseña*" autocomplete="new-password" required=""
                           aria-required="true" >
                </div>
            </div>

            <div class="row row-cols-1">
                <div class="col-12">
                    <input type="text" class="form-control" name="usu_nombre" placeholder="Nombre*" required=""
                           aria-required="true">
                </div>
                <div class="col-lg-6">
                    <input type="text" class="form-control" name="usu_ape1" placeholder="Primer apellido*"
                           required=""
                           aria-required="true">
                </div>
                <div class="col-lg-6">
                    <input type="text" class="form-control" name="usu_ape2" placeholder="Segundo apellido*"
                           required=""
                           aria-required="true">
                </div>
                <div class="col-lg-6">
                    <input type="text" class="form-control" name="nif" placeholder="NIF / NIE">
                </div>
                <div class="col-lg-6">
                    <select name="usu_codperfil" id="usu_codperfil" class="form-control">
                        <option value="" disabled selected>Eres*</option>
                        <option value="ME">Médico</option>
                        <option value="FA">Farmacéutico</option>
                        <option value="AX">Auxiliar de Farmacia</option>
                        <option value="OT">Otros</option>
                        <option value="RS">Residente</option>
                        <option value="EN">Enfermero</option>
                    </select>
                </div>
                <div class="col-lg-12">
                    <input type="text" class="form-control" name="empresa" placeholder="Centro de trabajo">
                </div>

                <div class="col-lg-6">
                    <select class="form-control" name="usu_codpais" id="usu_codpais" autocomplete="off">
                        <option value="">Pais*</option>
                        <option value="ADH" selected>Abu Dhabi</option>
                        <option value="660">Afganistan</option>
                        <option value="070">Albania</option>
                        <option value="004">Alemania</option>
                        <option value="043">Andorra</option>
                        <option value="330">Angola</option>
                        <option value="AIA">Anguila</option>
                        <option value="451">Antigua &amp; Barbuda</option>
                        <option value="476">Antillas Holandesas</option>
                        <option value="632">Arabia Saudita</option>
                        <option value="208">Argelia</option>
                        <option value="528">Argentina</option>
                        <option value="ABW">Aruba</option>
                        <option value="800">Australia</option>
                        <option value="038">Austria</option>
                        <option value="453">Bahamas</option>
                        <option value="640">Bahrain</option>
                        <option value="666">Bangladesh</option>
                        <option value="BRB">Barbados</option>
                        <option value="017">Bélgica</option>
                        <option value="421">Belize</option>
                        <option value="284">Benin</option>
                        <option value="413">Bermuda</option>
                        <option value="BLR">Bielorusia</option>
                        <option value="516">Bolivia</option>
                        <option value="391">Botswana</option>
                        <option value="508">Brasil</option>
                        <option value="703">Brunei</option>
                        <option value="068">Bulgaria</option>
                        <option value="236">Burkina Faso</option>
                        <option value="675">Butan</option>
                        <option value="KHM">Camboya</option>
                        <option value="302">Cameroun</option>
                        <option value="404">Canada</option>
                        <option value="512">Chile</option>
                        <option value="720">China</option>
                        <option value="600">Chipre</option>
                        <option value="480">Colombia</option>
                        <option value="318">Congo</option>
                        <option value="728">Corea del Sur</option>
                        <option value="272">Costa De Marfil</option>
                        <option value="436">Costa Rica</option>
                        <option value="092">Croacia</option>
                        <option value="448">Cuba</option>
                        <option value="CUR">Curaçao</option>
                        <option value="008">Dinamarca</option>
                        <option value="338">Djibuti</option>
                        <option value="DMA">Dominica</option>
                        <option value="DUB">Dubai</option>
                        <option value="500">Ecuador</option>
                        <option value="220">Egipto</option>
                        <option value="428">El Salvador</option>
                        <option value="647">Emiratos Arabes Unidos</option>
                        <option value="063">Eslovaquia</option>
                        <option value="091">Eslovenia</option>
                        <option value="042">España</option>
                        <option value="400">Estados Unidos</option>
                        <option value="053">Estonia</option>
                        <option value="334">Etiopia</option>
                        <option value="815">Fiji</option>
                        <option value="708">Filipinas</option>
                        <option value="032">Finlandia</option>
                        <option value="001">Francia</option>
                        <option value="314">Gabon</option>
                        <option value="252">Gambia</option>
                        <option value="276">Ghana</option>
                        <option value="GIB">Gibraltar</option>
                        <option value="GRD">Granada</option>
                        <option value="009">Grecia</option>
                        <option value="GRL">Groenlandia</option>
                        <option value="GLP">Guadalupe</option>
                        <option value="808">Guam</option>
                        <option value="416">Guatemala</option>
                        <option value="310">Guinea Ecuatorial</option>
                        <option value="488">Guyana</option>
                        <option value="452">Haiti</option>
                        <option value="003">Holanda</option>
                        <option value="424">Honduras</option>
                        <option value="740">Hong Kong</option>
                        <option value="064">Hungría</option>
                        <option value="664">India</option>
                        <option value="700">Indonesia</option>
                        <option value="616">Iran</option>
                        <option value="612">Iraq</option>
                        <option value="007">Irlanda</option>
                        <option value="024">Islandia</option>
                        <option value="244">Islas Anglonormandas</option>
                        <option value="463">Islas Caiman</option>
                        <option value="814">Islas Cook</option>
                        <option value="FRO">Islas Faroe</option>
                        <option value="806">Islas Salomon</option>
                        <option value="457">Islas Vírgenes (U.S.)</option>
                        <option value="VGB">Islas Vírgenes Británicas</option>
                        <option value="624">Israel</option>
                        <option value="005">Italia</option>
                        <option value="464">Jamaica</option>
                        <option value="732">Japón</option>
                        <option value="628">Jordania</option>
                        <option value="346">Kenia</option>
                        <option value="636">Kuwait</option>
                        <option value="684">Laos</option>
                        <option value="395">Lesotho</option>
                        <option value="054">Letonia</option>
                        <option value="604">Líbano</option>
                        <option value="268">Liberia</option>
                        <option value="216">Libia</option>
                        <option value="LIE">Liechtenstein</option>
                        <option value="055">Lituania</option>
                        <option value="102">Luxemburgo</option>
                        <option value="370">Madagascar</option>
                        <option value="701">Malaisia</option>
                        <option value="386">Malawi</option>
                        <option value="667">Maldivas</option>
                        <option value="232">Mali</option>
                        <option value="046">Malta</option>
                        <option value="MAR">Marruecos</option>
                        <option value="MTQ">Martinica</option>
                        <option value="373">Mauricio</option>
                        <option value="228">Mauritania</option>
                        <option value="412">México</option>
                        <option value="MCO">Monaco</option>
                        <option value="716">Mongolia</option>
                        <option value="MSR">Montserrat</option>
                        <option value="366">Mozambique</option>
                        <option value="676">Myanmar</option>
                        <option value="NAM">Namibia</option>
                        <option value="672">Nepal</option>
                        <option value="432">Nicaragua</option>
                        <option value="240">Niger</option>
                        <option value="NGA">Nigeria</option>
                        <option value="028">Noruega</option>
                        <option value="804">Nueva Zelanda</option>
                        <option value="649">Oman</option>
                        <option value="662">Pakistán</option>
                        <option value="440">Panamá</option>
                        <option value="801">Papua Nueva Guinea</option>
                        <option value="520">Paraguay</option>
                        <option value="504">Perú</option>
                        <option value="PYF">Polinesia Francesa</option>
                        <option value="060">Polonia</option>
                        <option value="040">Portugal</option>
                        <option value="401">Puerto Rico</option>
                        <option value="644">Qatar</option>
                        <option value="006">Reino Unido</option>
                        <option value="306">Republica Centroafricana</option>
                        <option value="061">República Checa</option>
                        <option value="322">Republica Democratica Congo</option>
                        <option value="460">República Dominicana</option>
                        <option value="724">Republica Popular De Corea</option>
                        <option value="REU">Reunión</option>
                        <option value="324">Ruanda</option>
                        <option value="066">Rumania</option>
                        <option value="RUS">Rusia</option>
                        <option value="SAB">Saba</option>
                        <option value="819">Samoa Occidental</option>
                        <option value="469">San Bartolomé</option>
                        <option value="EUS">San Eustaquio</option>
                        <option value="047">San Marino</option>
                        <option value="MAT">San Martín</option>
                        <option value="467">San Vicente &amp; las Granadinas</option>
                        <option value="LCA">Santa Lucía</option>
                        <option value="248">Senegal</option>
                        <option value="094">Serbia y Montenegro</option>
                        <option value="355">Seychelles</option>
                        <option value="264">Sierra Leona</option>
                        <option value="706">Singapur</option>
                        <option value="608">Siria</option>
                        <option value="342">Somalia</option>
                        <option value="669">Sri Lanka</option>
                        <option value="KNA">St Kitts &amp; Nevis</option>
                        <option value="390">Sudáfrica</option>
                        <option value="224">Sudan</option>
                        <option value="030">Suecia</option>
                        <option value="036">Suiza</option>
                        <option value="492">Surinam</option>
                        <option value="393">Swazilandia</option>
                        <option value="680">Tailandia</option>
                        <option value="736">Taiwan</option>
                        <option value="352">Tanzania</option>
                        <option value="280">Togo</option>
                        <option value="817">Tonga</option>
                        <option value="472">Trinidad &amp; Tobago</option>
                        <option value="212">Túnez</option>
                        <option value="052">Turquía</option>
                        <option value="350">Uganda</option>
                        <option value="524">Uruguay</option>
                        <option value="484">Venezuela</option>
                        <option value="690">Vietnam</option>
                        <option value="652">Yemen</option>
                        <option value="378">Zambia</option>
                        <option value="382">Zimbabwe</option>
                    </select>
                </div>
                <div class="col-lg-6" id="dv_provincia">
                    <select class="form-control" name="usu_codprovestado" id="usu_codprovestado"
                            autocomplete="off"
                            disabled="">
                        <option value="madrid">Provincia*</option>
                    </select>
                </div>
                <div class="col-lg-12">
                    <input class="form-control" name="address" placeholder="Dirección">
                </div>
                <div class="col-lg-6">
                    <input class="form-control" name="cp" placeholder="Código postal">
                </div>
                <div class="col-lg-6">
                    <input class="form-control" name="tlf" placeholder="Teléfono">
                </div>
                <div class="col-lg-6">
                    <select class="form-control" name="end_year" data-component="date" required>
                        <?php
                        if (isset($_GET['end_year']) && !empty($_GET['end_year'])) {
                            ?>
                            <option value="<?php echo $_GET['end_year'] ?>"
                                    selected><?php echo $_GET['end_year'] ?></option>
                            <?php
                        } else {
                            ?>
                            <option value="" disabled selected>Año finalización residencia *</option>
                            <?php
                        }
                        for ($year = 1950; $year <= date('Y'); $year++) {
                            if ($year == date('Y')) {
                                echo '<option value="' . $year . '">En curso</option>';
                            } else {
                                echo '<option value="' . $year . '">' . $year . '</option>';
                            }
                        }

                        ?>
                    </select>
                </div>
                <div class="col-lg-6">

                    <?php
                    if (isset($_GET['code']) && !empty($_GET['code'])) {
                        $code = $_GET['code'];
                        $readonly = 'readonly';
                    }
                    ?>
                    <input id="border-code" type="text" class="form-control" name="codeAccess"
                           placeholder="Copie el códido aquí"  required <?php echo $readonly; ?>
                            value=<?php echo $code; ?>>
                </div>
                <div class="col-md-12">
                    <div class="form-check">
                        <label class="form-check-label">
                            <input type="checkbox" class="form-check-input" value="" name="acepto" required>
                            <p style="margin-top: 30px;" class="letter-grey">He leído y
                                acepto el <a
                                        href="https://www.esteveagora.com/aviso-legal" target="_blank"
                                        title="Aviso Legal">aviso
                                    legal</a> y la <a href="https://www.esteveagora.com/privacidad" target="_blank"
                                                      title="Política de privacidad" class="info-privacy">política de
                                    privacidad</a>.</p>
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input type="checkbox" class="form-check-input" name="mailing" value="S">
                            <p style="margin-top: 30px;" class="letter-grey">Deseo recibir <a
                                        href="https://www.esteveagora.com/privacidad" target="_blank" title="">comunicaciones
                                    comerciales</a>.</p>
                        </label>
                    </div>

                </div>
                <div class="clearfix"></div>
                <div class="col text-right">
                    <button class="btn btn-danger" type="submit" id="btn-user-new"><i class="bi bi-arrow-right-circle"></i> CREAR CUENTA
                    </button>
                </div>
            </div>
        </form>

    </div>
</div>
