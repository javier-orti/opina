<?php include 'views/all/header.php';?>
<section class="h-100 gradient-form" id="block-login-user">
    <div class="container py-5 h-100">
        <div class="col-xl-10">
            <div class="row g-0">
                <div class="col-lg-12">
                    <div class="card-body p-md-5 mx-md-4">
                        <div class="container-fluid">
                        </div>
                        <form name="login" method="post" action="?action=login">

                            <p class="letter-grey">Para poder acceder al contenido, inicia sesión o crea una cuenta.</p>
                            <p class="letter-grey">Si ya estás dado de alta en ESTEVE introduce tu usuario y
                                contraseña.</p>
                            <div class="form-outline mb-2">
                                <label class="form-label letter-red" for="form2Example11 ">Introduce tu correo
                                    electrónico</label>
                                <input type="email" name="usu_email" class="form-control"
                                       placeholder="Correo electrónico *"/>
                            </div>
                            <div class="form-outline mb-2">
                                <label class="form-label letter-red" for="form2Example22">Introduce tu
                                    contraseña</label>
                                <input type="password" name="usu_password" class="form-control" placeholder="Contraseña *"/>
                                <div class="text-right mt-2">
                                    <a class="text-muted letter-grey" href="#!">¿Has olvidado la contraseña?</a>
                                </div>
                            </div>

                            <div class="text-left">
                                <button class="btn btn-danger mb-3"
                                        type="submit"><i class="bi bi-arrow-right-circle"></i> INICIAR SESIÓN
                                </button>
                                <br>

                            </div>
                        </form>
                        <button class="btn btn-default mb-3" style="color: red;background: white" id="btn-user-new"
                        ><i class="bi bi-arrow-right-circle"></i> CREAR CUENTA
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<div hidden id="end_year"><?php echo $_GET['end_year']; ?></div>
<div hidden id="code"><?php echo $_GET['code']; ?></div>

