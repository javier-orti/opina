<nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top p-0 pt-2 pb-2">
    <a href="#"><img src="assets/img/esteve-top.png" alt="proyecto-opina-logo" id="logo-esteve"></a>
    <div class="container-fluid text-right" id="pre-nav">
        <a href="#"><span>REGISTRATE</span></a>
        <?php
        if ($_SESSION['nombre'] == 1) {
            ?>
            <h6 class="pt-2"><a class="mr-3" href="#" style="color: white;" id="register-user">Regístrate</a>
                <a href="#" style="color: white;" id="login-user-now">Login<i class="bi bi-door-open"></i></a></h6>

            <?php
        } else {
            ?>
            <div class="text-right">

                <h6 class="pt-2 mr-4"><a href="#" style="color: white;" id="info-user"><?php
                        if ($_SESSION['invitations'] && ($_SESSION['usu_level'] == 1 || $_SESSION['usu_level'] == 2) ) {
                            echo $_SESSION['invitations'] . ' usuarios  han sido invitados..  ';
                        }


                        echo '<b>Dr. ' . $_SESSION['nombre'] . '</b>'; ?></a>
                    <button class="btn btn-dark btn-sm pt-0 pb-0 ml-4" id="logout-user"><i
                                class="bi bi-box-arrow-left"></i>Logout
                    </button>
                </h6>
            </div>
            <?php
        }
        ?>
    </div>
    <button class="navbar-toggler mt-4 mr-3" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav ml-auto my-2 my-lg-0 navbar-nav-scroll text-center pt-4 mr-5">
            <li class="nav-item ">
                <a class="nav-link active" href="/opina">HOME<span class="sr-only">(current)</span></a>
            </li>


            <li class="nav-item">
                <?php if ($_SESSION['nombre'] == 1) {
                    ?>
                    <a class="nav-link " href="/opina" id="login-user">BIBLIOGRAFÍA</a>
                    <?php
                } else {
                    ?>
                    <a class="nav-link" href="/opina#block-home-2" id="bibliografia">BIBLIOGRAFÍA</a>
                    <?php
                } ?>
            </li>

            <li class="nav-item">
                <?php if ($_SESSION['nombre'] == 1) {
                    ?>
                    <a class="nav-link" href="/opina" id="login-user">CUESTIONARIO</a>
                    <?php
                } else {
                    ?>
                    <a class="nav-link questionnaire" href="#">CUESTIONARIO</a>
                    <?php
                } ?>

            </li>
            <li class="nav-item">
                <?php if ($_SESSION['nombre'] == 1) {
                    ?>
                    <a class="nav-link" href="/opina" id="login-user">INVITAR</a>
                    <?php
                } else {
                    ?>
                    <a class="nav-link " href="?actionQ=info">INVITAR</a>
                    <?php
                } ?>
            </li>
            <li class="nav-item">
                <?php if ($_SESSION['nombre'] == 1) {
                    ?>
                    <a class="nav-link" href="/opina" id="login-user">BASES DEL SORTEO</a>
                    <?php
                } else {
                    if ($_SESSION['usu_level'] == 2) {
                        ?>
                        <a class="nav-link " href="#">BASES DEL SORTEO</a>
                        <?php
                    }
                } ?>
            </li>
        </ul>
    </div>
</nav>

<div class="banner py-2">
    <div class="container">
        <div class="row row-cols-1 row-cols-lg-2">
            <div class="col">
                <img src="assets/img/banner-logo.png" class="w-100 p-5 img-responsive" alt="proyecto-opina-logo">
            </div>
            <div class="col mt-5">
                <img src="assets/img/banner-draw.png" class="w-75 mt-5 img-responsive" alt="proyecto-opina-dibujo">
            </div>
        </div>
    </div>
</div>



