<div id="body-opina">

    <?php

    if (isset($_SESSION['usu_level'])) {
        ?>

        <section id="block-home">
            <div class="container-fluid mt-4">

                <?php
                if ($_SESSION['invitations'] && ($_SESSION['usu_level'] == 1 || $_SESSION['usu_level'] == 2)) {
                    ?>
                    <div class="container mt-3 mb-3">
                        <button class="btn btn-danger btn-block btn-users-invitation p-2">
                            <?php echo $_SESSION['invitations']; ?> de tus invitados han respondido al cuestionario
                            OpinA
                        </button>
                    </div>

                    <div class="container" id="table-users-invitations" style="display: none;">
                        <table class="table ">
                            <thead>
                            <th>Nombre</th>
                            <th>Apellidos</th>
                            <th>E-mail</th>
                            </thead>
                            <tbody>
                            <?php
                            foreach ($_SESSION['usersInvitation'] as $users) {
                                ?>
                                <tr>
                                    <td><?php echo $users['nombre']; ?></td>
                                    <td><?php echo $users['ape1'] . " " . $users['ape2']; ?></td>
                                    <td><?php echo $users['email']; ?></td>
                                </tr>
                                <?php
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                    <?php
                }
                ?>
                <div class="text-center"><p class="letter-red">Bienvenido</p></div>
                <div class="text-center ml-5 mr-5 p-4">
                    <p class="letter-grey">Está ampliamente demostrada la importancia del manejo adecuado
                        de las dislipidemias tanto en prevención primaria como en secundaria y su impacto en el
                        desarrollo
                        de
                        las Enfermedades Cardiovasculares. Resulta fundamental que los profesionales de la salud
                        implicados
                        en
                        el manejo de estos pacientes adopten las prácticas más adecuadas basándose en la actualidad
                        científica y
                        el perfil del paciente con el fin de mejorar la calidad de la atención sanitaria.</p>
                    <p class="letter-grey">
                        El objetivo de este proyecto es evaluar el nivel de conocimiento por parte de Endocrinos y
                        especialistas
                        en Medicina Interna de las últimas evidencias y actualizaciones en el tratamiento del
                        paciente
                        dislipidémico.</p>
                </div>
            </div>
            <div class="container-fluid  block2">
                <div class="text-center p-3">
                    <p class="letter-red">¿Cómo participar?</p>
                    <div class="row mt-5 mr-5 ml-5 mb-3">
                        <div class="col-md-4 ">
                            <a href="#"><img src="assets/img/Componente%2034%20–%201.svg">
                                <br><br>
                                <p class="letter-grey">Lee los <b>3 artículos</b></p>
                            </a>
                        </div>
                        <div class="col-md-4">
                            <a href="#"><img src="assets/img/Componente%2035%20–%201.svg">
                                <br><br>
                                <p class="letter-grey"><b>Danos tu opinión</b></p>
                            </a>
                        </div>
                        <div class="col-md-4">
                            <a href="#"><img src="assets/img/Componente%2036%20–%201.svg">
                                <br><br>
                                <?php if ($_SESSION['usu_level'] == 1) { ?>
                                    <p class="letter-grey">Al finalizar el cuestionario <b>Invita</b> a un mínimo de <b>3
                                            colegas adjuntos</b></p>
                                <?php } else { ?>
                                    <p class="letter-grey">Al finalizar el cuestionario <b>Invita a más colegas</b>
                                        adjuntos</b></p>
                                <?php } ?>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid" id="block-home-2">
                <p class="letter-red text-center mt-5">Lecturas para responder cuestionario</p>
                <div class="container mt-4">
                    <p class="text-center letter-grey">Te invitamos a consultar los siguientes artículos para
                        posteriormente
                        dar tu opinión.<br> Cada uno se
                        acompaña de un visual abstract que te facilitará la revisión de los contenidos más
                        destacados.
                    </p>
                </div>
                <div class="m-5">
                    <div class="row m-5">
                        <div class="col-md-4 m-0">
                            <a href="#">
                                <img src="assets/img/cuadri.png">
                            </a>
                            <p class="mt-3"><span>TÍTULO ESTUDIO 1</span></p>
                            <button class="btn-article">IR AL ARTÍCULO</button>
                        </div>
                        <div class="col-md-4 m-0 ">
                            <a href="#">
                                <img src="assets/img/cuadri.png">
                            </a>
                            <p class="mt-3"><span>TÍTULO ESTUDIO 2</span></p>
                            <button class="btn-article">IR AL ARTÍCULO</button>
                        </div>
                        <div class="col-md-4  m-0">
                            <a href="#">
                                <img src="assets/img/cuadri.png">
                            </a>
                            <p class="mt-3"><span>TÍTULO ESTUDIO 3</span></p>
                            <button class="btn-article">IR AL ARTÍCULO</button>
                        </div>
                    </div>
                </div>
                <?php if (!$_SESSION['invitations']) {
                    ?>
                    <div class="text-center m-5">
                        <a style="text-decoration: none;" href="?actionQ=list">
                            <button class="btn btn-danger btn-block btn-questionnaire-home">RESPONDER AL CUESTIONARIO
                            </button>
                        </a>
                    </div>
                <?php } ?>
            </div>
        </section>
    <?php } else {
        ?>
        <section class="h-100 gradient-form " id="block-login-user">
            <div class="container text-center  pt-5">
                <p class="letter-red mb-5  ">Proyecto Opina</p>
                <p class="letter-grey">El objetivo de este proyecto es evaluar el nivel de
                    conocimiento por parte de Endocrinos y especialistas en Medicina interna de las
                    últimas evidencias y actualizaciones en el tratamiento del paciente
                    dislipidémico.</p>
            </div>


            <div class="container py-5 h-100">
                <div class="col-xl-10">
                    <div class="row g-0">
                        <div class="col-lg-12">
                            <div class="card-body p-md-5 mx-md-4">
                                <form name="login-code" method="get">
                                    <input type="hidden" name="action" value="login-code">
                                    <div class="form-outline mb-2">
                                        <label class="form-label letter-red" for="form2Example11 ">Introduce tu
                                            código</label>
                                        <input type="text" name="code" class="form-control"
                                               placeholder="Código *"/>
                                    </div>
                                    <div class="form-outline mb-2">
                                        <label class="form-label letter-red" for="form2Example22">Introduce tu año
                                            de
                                            residencia</label>
                                        <select class="form-control" name="end_year" data-component="date" required>
                                            <option value="" disabled selected>Año finalización residencia *
                                            </option>
                                            <?php
                                            for ($year = 1950; $year <= date('Y'); $year++) {
                                                if ($year == date('Y')) {
                                                    echo '<option value="' . $year . '">En curso</option>';
                                                } else {
                                                    echo '<option value="' . $year . '">' . $year . '</option>';
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="text-left">
                                        <button class="btn btn-danger mb-3"
                                                type="submit"><i class="bi bi-arrow-right-circle"></i> SIGUENTE
                                        </button>
                                        <br>

                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <?php

    } ?>

</div>

