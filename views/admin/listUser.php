<div class="container table-responsive mt-2 mb-2">
    <div class="text-center">
        <h3>USUARIOS REGISTRADOS</h3>
    </div>
    <table class="table table-list-user">
        <thead>
        <th>Nº</th>
        <th>Nombre</th>
        <th>Apellidos</th>
        <th>E-mail</th>
        <th>Nivel</th>
        <th>Código de acceso</th>
        <th>Código embajador</th>
        <th>Año finalizado</th>
        <th>Cuestionario enviado</th>
        <th class="text-right">Opciones</th>
        </thead>
        <tbody>
        <?php
        $count = 1;
        foreach ($users as $q) {
            ?>
            <tr>
                <td><?php echo $count; ?></td>
                <td><?php echo $q['nombre']; ?></td>
                <td><?php echo $q['ape1'] . " " . $q['ape2']; ?></td>
                <td><?php echo $q['email']; ?></td>
                <td><?php
                    if ($q['usu_level'] == 0) {
                        echo "<span style='color: blue;'>administrador</span>";
                    } else {
                        echo $q['usu_level'];
                    }
                    ?></td>
                <td><span style="color: green"><?php echo $q['code_access']; ?></span></td>
                <td><span style="color: darkslategrey"><?php echo $q['codembajador']; ?></span></td>
                <td><span style="color: darkslategrey"><?php echo $q['year']; ?></span></td>
                <td><?php
                    $response=list_users_send($q['id']);
                    if($response){
                        echo $response['created_date'];
                    }else{
                        echo "No enviado";
                    }
                    ?></td>
                <td class="text-right">
                    <?php if ($q['usu_level'] != 0) {
                        ?>
                        <a href="?actionA=edit-user&id=<?php echo $q['id']; ?>" style="color: black;font-size: 1.9em;"
                           title="Edita"><i class="bi bi-gear"></i></i></a>
                    <?php } ?>
                </td>

            </tr>
            <?php
            $count++;
        }
        ?>
        </tbody>
    </table>
</div>

<script>
    $(document).ready(function () {
        $('.table-list-user').DataTable();
    });
</script>