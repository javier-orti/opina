<div class="container table-responsive mt-2 mb-2">
    <div class="text-center">
        <h3>PREGUNTAS DEL CUESTIONARIO</h3>
    </div>
    <table class="table table-list-questions">
        <thead>
        <th>Nº</th>
        <th>Preguntas</th>
        <th class="text-right"><a href="#" style="color: forestgreen;font-size: 1.9em;" title="Añadir " id="add-question-admin"><i class="bi bi-plus-circle"></i></a></th>
        </thead>
        <tbody>
        <?php
        $count=1;
        foreach ($questions as $q) {
            ?>
            <tr>
                <td><?php echo $count;?></td>
                <td><?php echo $q['question']; ?></td>
                <td class="text-right">
                    <a href="?actionA=edit-question&id=<?php echo $q['id']; ?>" style="color: black;font-size: 1.9em;" title="Edita"><i class="bi bi-gear"></i></i></a>
                </td>
            </tr>
            <?php
            $count++;
        }
        ?>
        </tbody>
    </table>
</div>

<script>
    $(document).ready(function() {
        $('.table-list-questions').DataTable();
    } );
</script>


