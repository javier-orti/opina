<div class="container mt-2 mb-2">
    <fieldset class="p-2">
        <div class="text-center"><h4><b>AÑADIR</b></h4></div>
        <h6>PREGUNTA</h6>
        <form method="post" name="add-question">
            <div class="row">
                <div class="col-md-12">
                    <textarea name="question" class="form-control" rows=5
                              placeholder="Escribe aquí la pregunta"></textarea>
                </div>
            </div>
            <hr>
            <h6>RESPUESTAS</h6>
            <a href="#" style="color: black;font-size: 1.9em;" title="Añadir respuestas" id="add-answer-admin"><i
                        class="bi bi-plus-circle"></i></a>
            <a href="#" style="color: black;font-size: 1.9em;" title="Añadir respuestas" id="delete-answer-admin"><i class="bi bi-dash-circle"></i></a>

            <div id="content-answers"></div>
            <div class="mt-2 text-center">
                <button class="btn btn-success btn-lg " type="submit">AGREGAR</button>
            </div>
        </form>
    </fieldset>
</div>