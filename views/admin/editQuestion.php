<div class="container mt-2 mb-2">
    <fieldset class="p-2">
        <div class="text-center"><h4><b>EDITAR</b></h4></div>
        <h6>PREGUNTA</h6>
        <form method="post" name="update-question-form">
            <input name="id" type="hidden" value="<?php echo $question['id'];?>">
            <div class="row">
                <div class="col-md-12">

                    <textarea name="<?php echo $question['id'];?>" class="form-control" rows=5
                              placeholder="Escribe aquí la pregunta"><?php echo $question['question']; ?></textarea>
                </div>
            </div>
            <hr>
            <h6>RESPUESTAS</h6>

            <div id="content-answers">
                <?php
                foreach ($answers as $answer) {
                    ?>
                    <div class="ans row mt-2">
                        <div class="col-md-12">
                                              <textarea name="<?php echo $answer['id'];?>" class="form-control" rows=2
                                                        placeholder="Escribe aquí la respuesta"><?php echo $answer['answer']; ?></textarea>
                        </div>
                    </div>
                    <?php
                }
                ?>
            </div>
            <div class="mt-2 text-center">
                <button class="btn btn-success btn-lg " type="submit">MODIFICA</button>
            </div>
        </form>
    </fieldset>
</div>