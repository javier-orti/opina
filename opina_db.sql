-- MySQL dump 10.13  Distrib 8.0.27, for Linux (x86_64)
--
-- Host: localhost    Database: opina_db
-- ------------------------------------------------------
-- Server version	8.0.27-0ubuntu0.20.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `answers`
--

DROP TABLE IF EXISTS `answers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `answers` (
  `id` int NOT NULL AUTO_INCREMENT,
  `answer` varchar(4000) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `id_question` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `answers_FK` (`id_question`),
  CONSTRAINT `answers_FK` FOREIGN KEY (`id_question`) REFERENCES `questionnaire` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_spanish2_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `answers`
--

LOCK TABLES `answers` WRITE;
/*!40000 ALTER TABLE `answers` DISABLE KEYS */;
INSERT INTO `answers` VALUES (23,'Cu nam labores lobortis definiebas, ei aliquyam salutatus persequeris quo, cum eu nemore fierent dissentiunt. Per vero dolor id, vide democritum scribentur eu vim, pri erroribus temporibus ex. Euismod molestie offendit has no. Quo te semper invidunt quaestio, per vituperatoribus sadipscing ei, partem aliquyam sensibus in cum.',15),(24,'Cu nam labores lobortis definiebas, ei aliquyam salutatus persequeris quo, cum eu nemore fierent dissentiunt. Per vero dolor id, vide democritum scribentur eu vim, pri erroribus temporibus ex. Euismod molestie offendit has no. Quo te semper invidunt quaestio, per vituperatoribus sadipscing ei, partem aliquyam sensibus in cum.',15),(25,'Cu nam labores lobortis definiebas, ei aliquyam salutatus persequeris quo, cum eu nemore fierent dissentiunt. Per vero dolor id, vide democritum scribentur eu vim, pri erroribus temporibus ex. Euismod molestie offendit has no. Quo te semper invidunt quaestio, per vituperatoribus sadipscing ei, partem aliquyam sensibus in cum.',15),(26,'un gusano',16),(27,'un insecto vivo',16),(28,'una hormiga',16),(29,'Cu nam labores lobortis definiebas, ei aliquyam salutatus persequeris quo, cum eu nemore fierent dissentiunt. Per vero dolor id, vide democritum scribentur eu vim, pri erroribus temporibus ex. Euismod molestie offendit has no. Quo te semper invidunt quaestio, per vituperatoribus sadipscing ei, partem aliquyam sensibus in cum.',17),(30,'una película',17),(31,'un juguete',17),(32,'un juego',18),(33,'Cu nam labores lobortis definiebas, ei aliquyam salutatus persequeris quo, cum eu nemore fierent dissentiunt. Per vero dolor id, vide democritum scribentur eu vim, pri erroribus temporibus ex. Euismod molestie offendit has no. Quo te semper invidunt quaestio, per vituperatoribus sadipscing ei, partem aliquyam sensibus in cum.',18),(34,'una manzana',18);
/*!40000 ALTER TABLE `answers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `com_alumnos`
--

DROP TABLE IF EXISTS `com_alumnos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `com_alumnos` (
  `id` int NOT NULL AUTO_INCREMENT,
  `ape1` text,
  `ape2` text,
  `codusuario` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `email` text,
  `nombre` text,
  `dni` text,
  `perfil` text,
  `especialidad` varchar(100) DEFAULT NULL,
  `numcolegiado` varchar(100) DEFAULT NULL,
  `pais` varchar(100) DEFAULT NULL,
  `provincia` varchar(100) DEFAULT NULL,
  `poblacion` varchar(100) DEFAULT NULL,
  `ciudad` varchar(200) DEFAULT NULL,
  `direccion` text,
  `cp` varchar(100) DEFAULT NULL,
  `telefono` text,
  `fax` text,
  `empresa` text,
  `usu_level` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `clave` text,
  `pass` text,
  `tipo_test` text,
  `servicio` int DEFAULT '0',
  `activado` int DEFAULT '0',
  `codembajador` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `year` int DEFAULT NULL,
  `code_access` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=99 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `com_alumnos`
--

LOCK TABLES `com_alumnos` WRITE;
/*!40000 ALTER TABLE `com_alumnos` DISABLE KEYS */;
INSERT INTO `com_alumnos` VALUES (48,'orti','marina','48OPR8PY','lucas@gmail.com','lucas',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1',NULL,'$2y$10$AQww7orfuYnX4rVBaZaXQe/Ppcw2xEIiDJLxWedK8MQAK8LJhQsky',NULL,0,1,'7IA4OP48',1970,'12345'),(49,'orti','','','admin@admin.com','javier',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0',NULL,'$2y$10$T3SqyJ43XRSgeKor3yn/puFlUrU0frveNX0zafJJJ0XenqT3UhYfO',NULL,0,0,'',NULL,NULL),(95,'orti','marina',NULL,'paco@gmail.com','paco',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2',NULL,'$2y$10$hmo5VM7VWcxGvGeQ2DIx6OjkAtN40eLTonc1l1nzurfAwMp.8zn8u',NULL,0,0,'52KGOP95',1987,'7IA4OP48'),(97,'efewf','ewf',NULL,'martin@gmail.com','efde',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2',NULL,'$2y$10$BflN0jrNANE9elx8WpyAO.8hV16Af3wN60B81nSmwtPfg6HMfe/HO',NULL,0,0,'NQAUOP97',1969,'52KGOP95'),(98,'ewr','rewr',NULL,'bartolo@gmail.com','erew',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2',NULL,'$2y$10$K67HLsAEEXS8hRbdklIcQe0ov8Az5FifQKavZ8bytk/2GfG3Igvoq',NULL,0,0,'XBL5OP0',1969,'NQAUOP97');
/*!40000 ALTER TABLE `com_alumnos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `questionnaire`
--

DROP TABLE IF EXISTS `questionnaire`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `questionnaire` (
  `id` int NOT NULL AUTO_INCREMENT,
  `question` varchar(5000) COLLATE utf8_spanish2_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_spanish2_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `questionnaire`
--

LOCK TABLES `questionnaire` WRITE;
/*!40000 ALTER TABLE `questionnaire` DISABLE KEYS */;
INSERT INTO `questionnaire` VALUES (15,'¿Cuántos pacientes ves a la semana en tu consulta que podrían beneficiarse de esta característica de pitavastatina?'),(16,'¿Cuántos pacientes ves a la semana en tu consulta que podrían beneficiarse de esta característica de pitavastatina?'),(17,'¿que son  las mates?'),(18,'¿que es el parchis?');
/*!40000 ALTER TABLE `questionnaire` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `response_questions`
--

DROP TABLE IF EXISTS `response_questions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `response_questions` (
  `id` int NOT NULL AUTO_INCREMENT,
  `id_alum` int DEFAULT NULL,
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `response_questions` json NOT NULL,
  PRIMARY KEY (`id`),
  KEY `response_questions_FK` (`id_alum`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_spanish2_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `response_questions`
--

LOCK TABLES `response_questions` WRITE;
/*!40000 ALTER TABLE `response_questions` DISABLE KEYS */;
INSERT INTO `response_questions` VALUES (26,48,'2021-12-31 17:59:12','[{\"1\": \"1\"}, {\"2\": \"5\"}]'),(28,81,'2022-01-02 17:58:53','[{\"15\": \"24\"}, {\"16\": \"27\"}]'),(29,83,'2022-01-03 13:51:11','[{\"15\": \"24\"}, {\"16\": \"27\"}]');
/*!40000 ALTER TABLE `response_questions` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-01-15 18:04:50
