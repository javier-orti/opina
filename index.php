<?php
session_start();
if (!isset($_SESSION['nombre'])) {
    $_SESSION['nombre'] = 1;
}
require_once 'core/core.php';
require_once 'views/all/header.php';
require_once 'views/all/nav.php';
if (empty($_GET['actionQ'])) {
    require_once 'views/all/body.php';
}


//TODO: functions for USER

if (isset($_GET['share'])) {
    include "views/home/user_register.php";
}

include "controllers/UserController.php";
if (isset($_GET['action']) && !empty(isset($_GET['action']))) {
    $action = $_GET['action'];

    switch ($action) {
        case "register":
            {
                 $actionLogin = save_user();
                 if($actionLogin=='success'){
                     login_user($_POST['usu_email'],$_POST['usu_password']);
                 }
            }
            break;
        case "login-check":
            {

                if ($_SESSION['nombre'] == 1) {//if login no success
                    ?>
                    <script>
                        // alert("Comprueba correo y contraseña.")
                        $('#body-opina').load('views/home/user_login.php');  </script><?php
                }
                if (isset($_SESSION['registered']) && $_SESSION['registered'] == 0) {
                    ?>
                    <script> alert('Consigue un código de embajador de algún amigo para participar.');
                    logout();</script><?php

                }

            }
            break;
        case "login":
            {
             $res=login_user($_POST['usu_email'],$_POST['usu_password']);
             if($res=='KO'){
                 ?>
                 <script> location.href='?action=login-ckeck';</script><?php
             }


            }
            break;
        case "login-code":
            {

                if (search_code($_GET['code'], $_GET['end_year'])) {
                    $year = $_GET['end_year'];
                    $code = $_GET['code'];
                    ?>
                    <script>$('#body-opina').load('views/home/user_login.php?end_year=<?php echo $year ?>&code=<?php echo $code ?>');  </script><?php
                } else {
                    ?>
                    <script> alert('El código introducido no es correcto.'); </script><?php
                }
            }
            break;
        case "logout":
            {

                session_destroy(); // or call here two();
            }
            break;

        default:
        {

        }
    }
}

//TODO: functions for Questionnaire
if ($_SESSION['id']) {
    include "controllers/QuestionnaireController.php";
    if (isset($_GET['actionQ']) && !empty(isset($_GET['actionQ']))) {
        $action = $_GET['actionQ'];
        switch ($action) {
            case "list":
                {
                    $listQuestions = list_questionnaire();
                    if ($_SESSION['id'] && $_SESSION['usu_level'] != 0) {//if exists user and not admin
                        $checkForm = check_form($_SESSION['id']);//if send user form
                        if (!$checkForm) {
                            include "views/questionnaire/questionnaire.php";
                        } else {
                            ?>
                            <div class="text-center mt-2">
                                <h2 style="color:darkred">Ya has enviado el formulario anteriormente.</h2>
                            </div>
                            <?php
                        }
                    }
                }
                break;
            case "success":
                {

                    action_questionnaire($_SESSION['id']);
                }
                break;
            case "info":
                {
                    include 'views/home/info.php';
                }
                break;
            default:
            {
                // do not forget to return default data, if you need it...
            }
        }
    }
}


//TODO ONLY FOR ADMIN


if (isset($_SESSION['nombre']) && isset($_SESSION['usu_level']) && $_SESSION['usu_level'] == 0) {

    include "views/admin/menuAdmin.php";
    if (isset($_GET['actionA']) && !empty(isset($_GET['actionA']))) {
        $action = $_GET['actionA'];
        switch ($action) {
            case "questions":
                {
                    $questions = list_questionnaire();
                    include 'views/admin/listQuestions.php';

                }
                break;
            case "add":
                {
                    include 'views/admin/addQuestion.php';
                }
                break;
            case "save-question":
                {
                    insert_question_answers();
                }
                break;
            case "edit-question":
                {
                    if (isset($_GET['id']) && !empty(isset($_GET['id']))) {
                        $idQuestion = $_GET['id'];
                        $question = edit_question($idQuestion);
                        $answers = edit_answers($idQuestion);
                        include 'views/admin/editQuestion.php';

                    }

                }
                break;
            case "updateQuestionnarieform":
                {
                    $idQuestion = $_POST['id'];
                    $question = edit_question($idQuestion);
                    $answers = edit_answers($idQuestion);
                    update_question_answer($question, $answers);

                }
                break;
            case "list-users":
                {
                    $users = list_users();
                    include "views/admin/listUser.php";

                }
                break;
            default:
            {

            }
        }
    }

}
require_once 'views/all/footer.php';
?>